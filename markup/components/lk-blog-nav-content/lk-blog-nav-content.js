$('.burger_lk-nav').click(function() {
    if ($('.burger_lk-nav').hasClass('open')) {
        $(this).removeClass('open');
        $('.lk_nav-wrap').fadeOut(0);
        $('.lk_content').fadeIn(0);
    } else {
        $(this).addClass('open');
        $('.lk_nav-wrap').fadeIn(0);
        $('.lk_content').fadeOut(0);
        if ($('.burger_lk-info').hasClass('open')) {
            $('.burger_lk-info').removeClass('open');
            $('.lk_sidebar').fadeOut(0);
        }
    }
});

$('.burger_lk-info').click(function() {
    if ($('.burger_lk-info').hasClass('open')) {
        $(this).removeClass('open');
        $('.lk_sidebar').fadeOut(0);
        $('.lk_content').fadeIn(0);
    } else {
        $(this).addClass('open');
        $('.lk_sidebar').fadeIn(0);
        $('.lk_content').fadeOut(0);
        if ($('.burger_lk-nav').hasClass('open')) {
            $('.burger_lk-nav').removeClass('open');
            $('.lk_nav-wrap').fadeOut(0);
        }
    }
});