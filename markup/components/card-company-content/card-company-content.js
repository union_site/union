$('.card_info-tel_wrap').click(function() {
    $(this).nextAll('.card_info-tel').css('display', 'block')
    $(this).remove()
})


$('.fotorama').fotorama({
    width: '100%',
    allowfullscreen: false,
    nav: 'thumbs',
    thumbwidth: '80px',
    thumbheight: '80px',
    arrows: true,
    loop: true,
    shadows: false
});

$('.main_content-block_action-wrap').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '.slic-arrow.prev',
    nextArrow: '.slic-arrow.next',
    responsive: [
        {
            breakpoint: 950,
            settings: {
                slidesToShow: 3,
                arrows: true,
                prevArrow: '.slic-arrow.prev',
                nextArrow: '.slic-arrow.next'
            }
        },
        {
            breakpoint: 790,
            settings: {
                slidesToShow: 2,
                arrows: true,
                prevArrow: '.slic-arrow.prev',
                nextArrow: '.slic-arrow.next'
            }
        },
        {
            breakpoint: 580,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                prevArrow: '.slic-arrow.prev',
                nextArrow: '.slic-arrow.next'
            }
        }
    ]
});
