$('.burger_login').click(function() {
    if ($('.burger_login').hasClass('open')) {
        $(this).removeClass('open');
        $('.navbar').fadeOut(0);
        $('.main_content').fadeIn(0);

    } else {
        $(this).addClass('open');
        $('.navbar').fadeIn(0);
        $('.main_content').fadeOut(0);
    }
});

$('.burger_nav').click(function() {
    if ($('.burger_nav').hasClass('open')) {
        $(this).removeClass('open');
        $('.menu').fadeOut(0);
        $('.main_content').fadeIn(0);
        $('.catalog_content').fadeIn(0);
    } else {
        $(this).addClass('open');
        $('.menu').fadeIn(0);
        $('.main_content').fadeOut(0);
        $('.catalog_content').fadeOut(0);
        if ($('.burger_search').hasClass('open')) {
            $('.burger_search').removeClass('open');
            $('.main_sidebar').fadeOut(0);
        }
    }
});

$('.burger_search').click(function() {
    if ($('.burger_search').hasClass('open')) {
        $(this).removeClass('open');
        $('.main_sidebar').fadeOut(0);
        $('.main_content').fadeIn(0);
        $('.catalog_content').fadeIn(0);

    } else {
        $(this).addClass('open');
        $('.main_sidebar').fadeIn(0);
        $('.main_content').fadeOut(0);
        $('.catalog_content').fadeOut(0);
        if ($('.burger_nav').hasClass('open')) {
            $('.burger_nav').removeClass('open');
            $('.menu').fadeOut(0);
        }
    }
});
