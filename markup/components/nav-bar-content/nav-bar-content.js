$('.nav-bar_slider').on('init', function(event, slick) {
    $('.plus-arrow.prev').fadeOut(0)
});

$('.nav-bar_slider').slick({
    slidesToShow: 11,
    slidesToScroll: 10,
    arrows: true,
    infinite: false,
    dots: false,
    draggable: false,
    adaptiveHeight: false,
    prevArrow: '.plus-arrow.prev',
    nextArrow: '.plus-arrow.next'
})



$('.plus-arrow').click(function() {
    if ($('.nav-bar_item:last').hasClass('slick-active')) {
        $('.plus-arrow.next').fadeOut()
    } else {
        $('.plus-arrow.next').fadeIn()
    }
    if ($('.nav-bar_item:first').hasClass('slick-active')) {
        $('.plus-arrow.prev').fadeOut()
    } else {
        $('.plus-arrow.prev').fadeIn()
    }
});
