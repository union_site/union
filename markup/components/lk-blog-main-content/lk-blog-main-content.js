$('.lk_table').cardtable();

$('.choose-rating').click(function() {
    $('.popup-rating_wrap').css('display', 'flex');
})

$('.close-popup, .popup-rating_bg').click(function() {
    $('.popup-rating_wrap').fadeOut();
})

$("label[for='lk_rating-1']").click(function() {
	$('.popup-rating_1').fadeOut(0);
	$('.popup-rating_2').fadeIn();
})

$("label[for='lk_rating-2']").click(function() {
	$('.popup-rating_1').fadeOut(0);
	$('.popup-rating_3').fadeIn();
})

$("label[for='lk_call-1']").click(function() {
	$('.popup-rating_3').fadeOut(0);
	$('.popup-rating_4').fadeIn();
})

$("label[for='lk_call-2']").click(function() {
	$('.popup-rating_3').fadeOut(0);
	$('.popup-rating_5').fadeIn();
})

$("label[for='lk_reason-1'], label[for='lk_reason-2']").click(function() {
	$('.popup-rating_5').fadeOut(0);
	$('.popup-rating_2').fadeIn();
})

$("label[for='lk_reason-3']").click(function() {
	$('.popup-rating_5').fadeOut(0);
	$('.popup-rating_6').fadeIn();
})

$(".btn_popup-submit").click(function() {
	$('.popup-rating_6').fadeOut(0);
	$('.popup-rating_2').fadeIn();
})