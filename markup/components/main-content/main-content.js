$(function() {
    $(".main_select-city").selectmenu({
        select: function(event, ui) {
            if ($(this).val() == 10) {
                $('.popup-city, .popup-bg').fadeIn()
            }
        }
    });
    $(".select-input").selectmenu();
});

$('.close-popup, .popup-bg').click(function() {
    $('.popup-city, .popup-bg').fadeOut()
})

$('#select-city').click(function() {
    $('.popup-city, .popup-bg').fadeIn()
})

$(document).ready(function() {
    var top_show = 650;
    var delay = 1000;
    if ($(window).width() > 1024) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > top_show) {
                $('.btn-to-top').fadeIn();
            } else {
                $('.btn-to-top').fadeOut(100);
            }
        });
    }
    $('.btn-to-top').click(function() {
        $('body, html').animate({
            scrollTop: 0
        }, delay);
    });
});


$('.main_content-block_adress-btn').click(function () {
    $(this).next('.main_content-block_adress-wrap').slideToggle()    
})

$('.main_content-banner').click(function () {
    $(this).find('.main_content-banner_text').slideToggle()    
})