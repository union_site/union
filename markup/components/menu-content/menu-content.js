$('.menu_link_wrap').hover(function() {
    if ($(window).width() > 1024) {
        $(this).toggleClass('active')
        $(this).find('.menu_sub-link_wrap').toggleClass('active')
        var p = $(this).find('.menu_sub-link_wrap').offset().top
        var h = $(this).find('.menu_sub-link_wrap').height()
        var w = $(window).height() + $(window).scrollTop()
        if (p + h > w) {
            $(this).find('.menu_sub-link_wrap').css('top', 'auto').css('bottom', '0')
        } else if (p + h < w) {
            $(this).find('.menu_sub-link_wrap').css('top', '0').css('bottom', 'auto')
        }
    }
})

$('.menu_link_wrap').click(function() {
    if ($(window).width() < 1024) {
        $(this).find('.menu_link').removeAttr("href");
        if ($(this).hasClass('active')) {
            $(this).removeClass('active')
        } else {
            $('.menu_link_wrap').removeClass('active')
            $(this).addClass('active')
        }
    }
})

$('.menu_link_wrap').each(function() {
    var a = $(this).find('.menu_sub-link').length
    if (a > 10 && a < 21) {
        $(this).find('.menu_sub-link_container').css('column-count', '2')
    } else if (a > 20 && a <= 31) {
        $(this).find('.menu_sub-link_container').css('column-count', '3')
    } else if (a < 10) {
        $(this).find('.menu_sub-link_container').css('column-count', '1')
    }
})
